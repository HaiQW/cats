# -*- coding: utf-8 -*-
import os

import jieba
import numpy as np
import pandas as pd

def new_jieba_fenci(raw_data, stopwords, pos_words, neg_words):
    """ Use the jieba fenci package to segment the sentence. 
    Then, remove such words that are in the stopwords_list.
    """
    for word in pos_words:
        jieba.add_word(word)
    for word in neg_words:
        jieba.add_word(word)
    
    orig_words = list(jieba.cut(raw_data, cut_all=False))
    words = [word for word in orig_words if (not word in stopwords) and (not word in [u' ', 'NULL', '/'])]
    words_with_stopwords = [word for word in orig_words if (not word in [u' ', 'NULL', '/'])]
    return words, words_with_stopwords

def jieba_fenci(raw_data, stopwords):
    """ Use the jieba fenci package to segment the sentence. 
    Then, remove such words that are in the stopwords_list.
    """
    orig_words = list(jieba.cut(raw_data, cut_all=False))
    words = [word for word in orig_words if (not word in stopwords) and (not word in [u' ', 'NULL', '/'])]
    words_with_stopwords = [word for word in orig_words if (not word in [u' ', 'NULL', '/'])]
    return words, words_with_stopwords


def load_stopwords(file_name):
    """ load the stopword set from the given file 
    """
    stopwords = []
    with open(file_name, "r") as fin:
        for line in fin.readlines():
            stopwords.append(line.decode("utf-8")[:-1])
    return stopwords


def split_dataset(item_comments):
    """Split all comments of each item."""
    results = []
    for item_index in item_comments.index:
        try:
            normal_comments =  item_comments.loc[item_index, 'normal_content'].decode('utf-8').split('/')
        except:
            normal_comments = ''
        try:
            spam_comments =  item_comments.loc[item_index, 'spam_content'].decode('utf-8').split('/')
        except:
            spam_comments = ''
           
        item_id, cnt, spam_cnt, normal_cnt = item_comments.loc[item_index, ['item_id', 'cnt', 'spam_cnt', 'normal_cnt']]
        for comment in normal_comments:
            results.append([item_id, cnt, spam_cnt, normal_cnt, comment, 0])
        for comment in spam_comments: 
            results.append([item_id, cnt, spam_cnt, normal_cnt, comment, 1])
    results = pd.DataFrame(data=results, columns=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'comment', 'is_spam'])
    return results


def load_orig_dataset(data_path, stopwords_path='/home/notebook/HaiQW/data/stopwords.txt'):
    orig_data = pd.read_csv(
        data_path,
        delimiter='|',
        error_bad_lines=False,
        header=None,
        names=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'spam_content', 'normal_content'])
    stopwords = load_stopwords(stopwords_path)
    
    count = 0
    for idx in range(0, orig_data.shape[0]):
        count = count + 1
        if (count % 10000 == 0):
            print ('cut all comments of the NO.%d item' % count)
        try:
            comment = orig_data.loc[idx, 'spam_content'] + '/' + orig_data.loc[idx, 'normal_content']
        except:
            # print idx
            # print orig_data.loc[idx, 'spam_content'], orig_data.loc[idx, 'normal_content']
            comment = ' '
        if comment.strip() != 'NULL' and comment.strip() != '':
            orig_data.loc[idx, 'comment'] = comment
            words, words_with_stopwords = jieba_fenci(comment, stopwords)
            orig_data.loc[idx, 'words'] = ' '.join(words)
            orig_data.loc[idx, 'words_with_stopwords'] = ' '.join(words_with_stopwords)
        else:
            orig_data.loc[idx, 'comment'] = np.nan
            orig_data.loc[idx, 'words'] = np.nan
            orig_data.loc[idx, 'words_with_stopwords'] = np.nan
    return orig_data


def load_and_split_dataset(data_path, stopwords_path='/home/notebook/HaiQW/data/stopwords.txt'):
    orig_data = pd.read_csv(
        data_path,
        delimiter='|',
        error_bad_lines=False,
        header=None,
        names=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'spam_content', 'normal_content'])
    orig_data = split_dataset(orig_data)
    stopwords = load_stopwords(stopwords_path)
    
    count = 0
    for idx in range(0, orig_data.shape[0]):
        count = count + 1
        if (count % 10000 == 0):
            print ('cut the NO.%d comment' % count)
        comment = orig_data.loc[idx, 'comment']
        if comment.strip() != 'NULL' and comment.strip() != '':
            orig_data.loc[idx, 'comment'] = comment
            words, words_with_stopwords = jieba_fenci(comment, stopwords)
            orig_data.loc[idx, 'words'] = ' '.join(words)
            orig_data.loc[idx, 'words_with_stopwords'] = ' '.join(words_with_stopwords)
        else:
            orig_data.loc[idx, 'comment'] = np.nan
            orig_data.loc[idx, 'words'] = np.nan
            orig_data.loc[idx, 'words_with_stopwords'] = np.nan
    return orig_data


def load_dataset_from_seperated_files(spam_file, normal_file, words_file, stopwords_file):
    """
    加载spam和normal的评论数据，并分词 
    """
    if os.path.isfile(words_file):
        items = pd.read_csv(words_file, error_bad_lines=False)
        words =  ' '.join(items['words'].dropna().tolist()).split(' ')
        return words
    
    orig_spam_items = pd.read_csv(spam_file,  delimiter='|', 
                                  names=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'spam_content', 'normal_content'], 
                                  error_bad_lines=False)
    orig_normal_items = pd.read_csv(normal_file,  delimiter='|', 
                                    names=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'spam_content', 'normal_content'],  
                                    error_bad_lines=False)
    stop_words = stopwords(stopwords_file)


    # proces orig dataset
    for idx in range(orig_spam_items.shape[0]):
        spam_content = orig_spam_items.loc[idx, "spam_content"].strip()
        normal_content = orig_spam_items.loc[idx, "normal_content"].strip()
        comment = spam_content + normal_content
        if comment:        
            orig_spam_items.loc[idx, 'comment'] = comment 
            word_set, words = jieba_fenci(comment.strip(), stop_words)
            orig_spam_items.loc[idx, 'words'] = ' '.join(words)
            
        else:
            orig_spam_items.loc[idx, 'comment'] = np.nan
            orig_spam_items.loc[idx, 'words'] = np.nan

    for idx in range(orig_normal_items.shape[0]):
        spam_content = orig_normal_items.loc[idx, "spam_content"].strip()
        normal_content = orig_normal_items.loc[idx, "normal_content"].strip()
        comment = spam_content + normal_content
        if comment:
            orig_normal_items.loc[idx, 'comment'] = comment 
            word_set, words = jieba_fenci(comment.strip(), stop_words)
            orig_normal_items.loc[idx, 'words'] = ' '.join(words)
        else:
            orig_normal_items.loc[idx, 'comment'] = np.nan
            orig_normal_items.loc[idx, 'words'] = np.nan

    # drop np.nan
    spam_items = orig_spam_items.dropna()
    normal_items = orig_normal_items.dropna()
    spam_items["label"] = 0
    normal_items["label"] = 1
    items = pd.DataFrame.append(spam_items, normal_items)
    items.to_csv(words_file, index=False, columns=['id', 'label', 'comment', 'words'], encoding='utf-8')   
    words =  ' '.join(items['words'].tolist()).encode('utf-8').split(' ')
    return words, items


def load_splitted_dataset_from_seperated_files(spam_file, normal_file):
    """
    加载spam和normal的评论数据，并分词 
    """    
    orig_spam_items = pd.read_csv(spam_file,  delimiter='|', 
                                  names=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'spam_content', 'normal_content'], 
                                  error_bad_lines=False)
    orig_normal_items = pd.read_csv(normal_file,  delimiter='|', 
                                    names=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'spam_content', 'normal_content'],  
                                    error_bad_lines=False)

    # proces orig dataset
    for idx in range(orig_spam_items.shape[0]):
        spam_content = orig_spam_items.loc[idx, "spam_content"].strip()
        normal_content = orig_spam_items.loc[idx, "normal_content"].strip()
        comment = spam_content + normal_content
        if comment:        
            orig_spam_items.loc[idx, 'comment'] = comment 
            word_set, words = jieba_fenci2(comment.strip())
            orig_spam_items.loc[idx, 'words'] = ' '.join(words)
            
        else:
            orig_spam_items.loc[idx, 'comment'] = np.nan
            orig_spam_items.loc[idx, 'words'] = np.nan

    for idx in range(orig_normal_items.shape[0]):
        spam_content = orig_normal_items.loc[idx, "spam_content"].strip()
        normal_content = orig_normal_items.loc[idx, "normal_content"].strip()
        comment = spam_content + normal_content
        if comment:
            orig_normal_items.loc[idx, 'comment'] = comment 
            word_set, words = jieba_fenci2(comment.strip())
            orig_normal_items.loc[idx, 'words'] = ' '.join(words)
        else:
            orig_normal_items.loc[idx, 'comment'] = np.nan
            orig_normal_items.loc[idx, 'words'] = np.nan

    # drop np.nan
    spam_items = orig_spam_items.dropna()
    normal_items = orig_normal_items.dropna()
    items = pd.DataFrame.append(spam_items, normal_items)
    words =  ' '.join(items['words'].tolist()).encode('utf-8').split(' ')
    return words, items


def build_codebook(word_set, 
                   dict_file='word2vec_dictionary.pkl', 
                   vocabulary_size=2000):
    """
    1.给'word_set'中出现过的单词做频数统计，取top 'vocabulary_size'频数的单词放入词典中，以便快速查询。
    2.评论词库'word_set'编码，出现在top 1999之外的单词，统一令其为'UNK'（未知）编号为0，并统计这些单词的数量。
    返回值: 评论词库编码data，每个单词的频数统计count，词汇表dictionary及其反转形式reverse_dictionary
    """
    # length of all counter:22159 取top1999频数的单词作为vocabulary，其他的作为unknown
    counter = collections.Counter(word_set).most_common(vocabulary_size-1)
    count = [['UNK', -1]]    #初始化单词频数统计集合
    count.extend(counter)

    dictionary = {}  #搭建dictionary
    for word, _ in count:
        dictionary[word] = len(dictionary)

    # 全部单词转为编号
    # 先判断这个单词是否出现在dictionary，如果是，就转成编号，如果不是，
    # 则转为编号0（代表UNK）
    data = []
    unk_count = 0
    for word in word_set:
        if word in dictionary:
            index = dictionary[word]
        else:
            index = 0
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(),dictionary.keys()))
    fout = open(dict_file, 'wb')  # save dict
    pickle.dump(reverse_dictionary, fout)
    return data, count, dictionary, reverse_dictionary 