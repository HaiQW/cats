#-*- coding:utf-8 -*-
import pickle
import exception

import nltk
import numpy as np
import pandas as pd
from snownlp import SnowNLP

__author__ = 'HaiQW'


def cal_single_comment_sentiment(comments):
    """calculate the ratios between positive words 
    and all words in each item's comments."""
    data_size = comments.shape[0]
    results = np.zeros(shape=(data_size, 1), dtype=np.float)

    count = 0
    for comment_idx in range(0, data_size):
        comment = comments.loc[comment_idx, 'comment']
        s = SnowNLP(comment)
        results[count, 0] = s.sentiments
        count += 1
    return results


def cal_item_comments_sentiments(comments):
    results = np.zeros(shape=(len(comments['item_id'].unique()), 2), dtype=np.float)
    
    count = 0
    for item_id in comments['item_id'].unique():
        item_comments = comments.loc[comments['item_id']==item_id, :]
        tmp = []
        for comment in item_comments['comment']:
            s = SnowNLP(comment)
            tmp.append(s.sentiments)
        results[count, 0] = item_id
        results[count, 1] = np.average(tmp)
        count +=1
    return results