#-*- coding:utf-8 -*-
import pickle
import exception

import nltk
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics.pairwise import pairwise_distances
from matplotlib import font_manager

from helper import jieba_fenci, load_and_split_dataset, load_orig_dataset


def cal_unique_words_ratio(comments):
    """calculate the ratios between unique words 
    and all words in each item's comments."""
    data_size = comments.shape[0]
    results = np.zeros(shape=(data_size, 2), dtype=float)
    
    count = -1
    for comment_idx in range(0, data_size):
        count += 1
        item_id = comments.loc[comment_idx, 'item_id']
        words = comments.loc[comment_idx, 'words']
        word_list = np.nan
        try:
            # case 1: 评论分词之后有内容
            word_list = words.split(' ')
        except:
            # case 2: 评论分词之后为空
            print('There exist no words in the comments.')
            results[count, 1] = np.nan
            results[count, 0] = item_id
            continue
        ratio =  float(len(set(word_list))) / len(word_list)
        results[count, 1] = ratio 
        results[count, 0] = item_id
    return results


def cal_positive_words_ratio(comments, words):
    """calculate the ratios between positive words 
    and all words in each item's comments."""
    data_size = comments.shape[0]
    ratios = np.zeros(shape=(data_size, 1), dtype=float)

    for comment_idx in range(0, data_size):
        comment1 = comments.loc[comment_idx, 'words1']
        comment2 = comments.loc[comment_idx, 'words2']
        word_list = np.nan
        try:
            # case 1: 评论分词之后有内容
            word_list1 = comment1.split(' ')
            word_list2 = comment2.split(' ')
        except:
            # case 2: 评论分词之后为空
            print('There exist no words in this comment.')
            ratio_list[comment_idx, 0] = np.nan
            continue
        positive_cnt = 0
        for word in word_list2:
            if word in words:
                positive_cnt += 1              
        ratios = positive_cnt / float(len(word_list2))
        ratios[comment_idx, 0] = ratio   
    return ratios


def cal_words_cnt(comments, words):
    """calculate the ratios between positive words 
    and all words in each item's comments."""
    data_size = comments.shape[0]
    cnts = np.zeros(shape=(data_size, 1), dtype=int)

    for comment_idx in range(0, data_size):
        comment = comments.loc[comment_idx, 'words']
        word_list = np.nan
        try:
            # case 1: 评论分词之后有内容
            word_list = comment.split(' ')
        except:
            # case 2: 评论分词之后为空
            print('There exist no words in this comment.')
            cnts[comment_idx, 0] = 0
            continue
        positive_cnt = 0
        for word in word_list:
            if word in words:
                positive_cnt += 1              
        cnts[comment_idx, 0] = positive_cnt   
    return cnts


def cal_ngram_ratios(comments, grams):
    """calculate the ratios between number of ngrams and all words
    in each comment"""
    data_size = comments.shape[0]
    result = np.zeros(shape=(data_size, 3), dtype=np.float)
    
    count = 0
    for comment_idx in comments.index:
        comment_id = comments.loc[comment_idx, 'item_id']
        try:
            tokens = comments.loc[comment_idx, 'words'].split()
            bigrams = list(nltk.bigrams(tokens))
            n_grams = np.sum([bigrams.count(g) for g in grams])
            if n_grams != np.nan and n_grams > 0:
                n_ratio = n_grams / float(len(bigrams))
            else:
                n_ratio = 0
        except:
            n_grams = 0
            n_ratio = 0
        result[count, 0] = comment_id
        result[count, 1] = float(n_grams)
        result[count, 2] = n_ratio
        count += 1
    return result
           

def cal_punctuation_ratios(comments):
    """cal the number of punctuations and the ratios 
    of punctions in each comment"""
    data_size = comments.shape[0]
    result = np.zeros(shape=(data_size, 3), dtype=np.float)
    
    count = 0
    for comment_idx in comments.index:
        comment_id = comments.loc[comment_idx, 'item_id']
        try:
            tokens = comments.loc[comment_idx, 'words_with_stopwords'].split()
            p_number = np.sum([tokens.count(p) for p in list([u'!', u'。', u',', u'?', u'，', u'.', u'~'])])
            if p_number != np.nan and p_number > 0:
                p_ratio = p_number/ float(len(tokens))
            else:
                p_ratio = 0
        except:
            p_number = 0
            p_ratio = 0
        result[count, 0] = comment_id
        result[count, 1] = p_number
        result[count, 2] = p_ratio
        count += 1
    return result


def cal_average_punctuation_ratios(comments):
    """cal the number of punctuations and the ratios 
    of punctions in each comment"""
    result = np.zeros(shape=(len(comments['item_id'].unique()), 3), dtype=np.float)
    
    count = 0
    for item_id in comments['item_id'].unique():
        item_comments = comments.loc[comments['item_id']==item_id, :]
        #comment_id = comments.loc[comment_idx, 'item_id']
        tmp_ratio = []
        tmp_number = []
        for comment in item_comments['words_with_stopwords']:
            try:
                tokens = comment.split()
                p_number = np.sum([tokens.count(p) for p in list([u'!', u'。', u',', u'?', u'，', u'.', u'~'])])
                if p_number != np.nan and p_number > 0:
                    p_ratio = p_number/ float(len(tokens))
                else:
                    p_ratio = 0
                    p_number = 0
                tmp_ratio.append(p_ratio)
                tmp_number.append(p_number)
            except:
                tmp_ratio.append(np.nan)
                tmp_number.append(np.nan)
        result[count, 0] = item_id
        result[count, 1] = np.nanmean(tmp_number)
        result[count, 2] = np.nanmean(tmp_ratio)
        count += 1
    return result


def cal_entropy(comments):
    """calculate the entropy of comments."""
    data_size = comments.shape[0]
    result = np.zeros(shape=(data_size, 2), dtype=np.float)
    
    count = 0
    for comment_idx in comments.index:
        comment_id = comments.loc[comment_idx, 'item_id']
        try:
            tokens = comments.loc[comment_idx, 'words_with_stopwords'].split()
            lf = nltk.FreqDist(tokens)
            entropy = nltk.probability.entropy(nltk.MLEProbDist(lf))
        except:
            entropy = np.nan
        result[count, 0] = comment_id
        result[count, 1] = entropy
        count += 1
    return result


def cal_average_entropy(comments):
    """calculate the entropy of comments."""
    data_size = comments.shape[0]
    result = np.zeros(shape=(len(comments['item_id'].unique()), 2), dtype=np.float)
    
    count = 0
    for item_id in comments['item_id'].unique():
        item_comments = comments.loc[comments['item_id']==item_id, :]
        tmp = []
        for comment in item_comments['words_with_stopwords']:
            try:
                tokens = comment.split(' ')
                lf = nltk.FreqDist(tokens)
                tmp.append(nltk.probability.entropy(nltk.MLEProbDist(lf)))
            except:
                tmp.append(np.nan)
        result[count, 0] = item_id
        result[count, 1] = np.nanmean(tmp)
        count += 1
    return result


def cal_average_comment_length(comments):
    """calculate the average length of comments in each item."""
    results = np.zeros(shape=(len(comments['item_id'].unique()), 3), dtype=np.float)
    
    count = 0
    for item_id in comments['item_id'].unique():
        item_comments = comments.loc[comments['item_id']==item_id, :]
        tmp = []
        for comment in item_comments['words_with_stopwords']:
            try:
                length = len(comment.split(' '))
                tmp.append(length)
            except:
                tmp.append(np.nan)
        results[count, 0] = item_id
        results[count, 1] = np.nanmean(tmp)
        results[count, 2] = np.nansum(tmp)
        count +=1
    return results  
    
"""   
def load_data1(data_path):
    orig_data = pd.read_csv(
        data_path,
        delimiter='|',
        error_bad_lines=False,
        header=None,
        names=['item_id', 'spam_cnt', 'normal_cnt', 'cnt', 'spam_content', 'normal_content'])
    orig_data = split_dataset(orig_data)
    stop_words = stopwords('/home/notebook/HaiQW/data/stopwords.txt')

    print('segment the comments ...')
    count = 0
    for idx in range(orig_data.shape[0]):
        count = count + 1
        if (count % 10000 == 0):
            print ('Cut comment %d' % count)
        comment = orig_data.loc[idx, 'comment']
        if comment.strip() != 'NULL' and comment.strip() != '':
            orig_data.loc[idx, 'comment'] = comment
            word_list1, word_list2 = jieba_fenci(comment, stop_words)
            orig_data.loc[idx, 'words1'] = ' '.join(word_list1)
            orig_data.loc[idx, 'words2'] = ' '.join(word_list2)
        else:
            orig_data.loc[idx, 'comment'] = np.nan
            orig_data.loc[idx, 'words1'] = np.nan
            orig_data.loc[idx, 'words2'] = np.nan
    return orig_data


def load_data2(data_path):
    orig_data = pd.read_csv(
        data_path,
        delimiter='|',
        error_bad_lines=False,
        header=None,
        names=['item_id', 'spam_cnt', 'normal_cnt', 'cnt', 'spam_content', 'normal_content'])
    # orig_data = split_dataset(orig_data)
    stop_words = stopwords('/home/notebook/HaiQW/data/stopwords.txt')

    print('segment the comments ...')
    count = 0
    for idx in range(0, orig_data.shape[0]):
        count = count + 1
        if (count % 10000 == 0):
            print ('Cut comment %d' % count)
        comment = orig_data.loc[idx, 'spam_content'] + orig_data.loc[idx, 'normal_content']
        if comment.strip() != 'NULL' and comment.strip() != '':
            orig_data.loc[idx, 'comment'] = comment
            word_list1, word_list2 = jieba_fenci(comment, stop_words)
            orig_data.loc[idx, 'words1'] = ' '.join(word_list1)
            orig_data.loc[idx, 'words2'] = ' '.join(word_list2)
        else:
            orig_data.loc[idx, 'comment'] = np.nan
            orig_data.loc[idx, 'words1'] = np.nan
            orig_data.loc[idx, 'words2'] = np.nan
    return orig_data


def load_data3(data_path):
    orig_data = pd.read_csv(
        data_path,
        delimiter='|',
        error_bad_lines=False,
        header=None,
        names=['item_id', 'spam_cnt', 'normal_cnt', 'cnt', 'spam_content', 'normal_content'])
    # orig_data = split_dataset(orig_data)

    print('segment the comments ...')
    count = 0
    for idx in range(0, orig_data.shape[0]):
        count = count + 1
        if (count % 10000 == 0):
            print ('Cut comment %d' % count)
        comment = orig_data.loc[idx, 'spam_content'] + orig_data.loc[idx, 'normal_content']
        if comment.strip() != 'NULL' and comment.strip() != '':
            orig_data.loc[idx, 'comment'] = comment
            word_list1, word_list2 = jieba_fenci2(comment)
            orig_data.loc[idx, 'words1'] = ' '.join(word_list1)
            orig_data.loc[idx, 'words2'] = ' '.join(word_list2)
        else:
            orig_data.loc[idx, 'comment'] = np.nan
            orig_data.loc[idx, 'words1'] = np.nan
            orig_data.loc[idx, 'words2'] = np.nan
    return orig_data


def load_data4(data_path):
    orig_data = pd.read_csv(
        data_path,
        delimiter='|',
        error_bad_lines=False,
        header=None,
        names=['item_id', 'spam_cnt', 'normal_cnt', 'cnt', 'spam_content', 'normal_content'])
    orig_data = split_dataset(orig_data)

    print('segment the comments ...')
    count = 0
    for idx in range(0, orig_data.shape[0]):
        count = count + 1
        if (count % 10000 == 0):
            print ('Cut comment %d' % count)
        comment = orig_data.loc[idx, 'comment']
        if comment.strip() != 'NULL' and comment.strip() != '':
            orig_data.loc[idx, 'comment'] = comment
            word_list1, word_list2 = jieba_fenci2(comment)
            orig_data.loc[idx, 'words1'] = ' '.join(word_list1)
            orig_data.loc[idx, 'words2'] = ' '.join(word_list2)
        else:
            orig_data.loc[idx, 'comment'] = np.nan
            orig_data.loc[idx, 'words1'] = np.nan
            orig_data.loc[idx, 'words2'] = np.nan
    return orig_data
"""