#-*- coding:utf-8 -*-
import pickle
import exception

import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import pairwise_distances

from helper import jieba_fenci, load_stopwords

__author__ = 'HaiQW'


def distance_matrix(embs):
    """计算各个word2vec的距离矩阵"""
    return pairwise_distances(embs, metric='cosine')


def encode_sentence(comments, codebook, embs, distance_matrix):
    """
    对每一个句子，构造一个n纬度对向量，
    向量每一维是该维对应的词和该句子中的每一个词的相似度的最大值
    """
    dim = embs.shape[1]
    dict_size = embs.shape[0]
    data_size = comments.shape[0]
    reverse_codebook = dict(zip(codebook.values(), codebook.keys()))
    vec = np.zeros(shape=(data_size, dict_size), dtype=float)

    count = 0
    for comment_idx in comments.index:
        if count % 10000 == 0:
            print ('encoding comment %d' % count)
        count += 1
        comment = comments[comment_idx]
        word_list = np.nan
        try:
            # case 1: 评论分词之后有内容
            word_list = comment.split(' ')
        except:
            # case 2: 评论分词之后为空
            print('There exist no words in this comment.')
            vec[comment_idx, :] = np.full(
                shape=(1, dict_size), fill_value=np.nan)
            continue

        comment_vec = np.zeros(shape=(1, dict_size), dtype=float)
        word_indices = []
        # 获取评论单词的编码
        for word in word_list:
            # 如果有word不在词库里，直接忽略这个word
            if reverse_codebook.has_key(word.encode('utf-8')):
                word_indices.append(reverse_codebook[word.encode('utf-8')])
        # 如果所有单词都不在词库里面，直接复制np.nan
        if len(word_indices) == 0:
            vec[comment_idx, :] = np.full(
                shape=(1, dict_size), fill_value=np.nan)
            continue
        # 开始编码
        for idx in range(0, dict_size):
            # case 1: 如果评论中出现过该单词，
            # 这个维度和词库embeddings的距离就为0
            # 那么这个维度的为零向量
            if codebook[idx].decode('utf-8') in word_list:
                comment_vec[0, idx] = 0
            # case 2: 如果评论中没有出现过该单词,
            # 取评论中所有单词中离该维度单词距离的最值（最大相似度）
            else:
                distance = np.min(distance_matrix[idx, word_indices])
            comment_vec[0, idx] = distance
        vec[comment_idx, :] = comment_vec
    return vec


def split_dataset(item_comments):
    """Split all comments of each item."""
    results = []
    for item_index in item_comments.index:
        normal_comments =  item_comments.loc[item_index, 'normal_comment'].decode('utf-8').split('/')
        spam_comments =  item_comments.loc[item_index, 'spam_comment'].decode('utf-8').split('/')
        item_id, cnt, spam_cnt, normal_cnt = item_comments.loc[item_index, ['item_id', 'cnt', 'spam_cnt', 'normal_cnt']]
        for comment in normal_comments:
            results.append([item_id, cnt, spam_cnt, normal_cnt, comment, 0])
        for comment in spam_comments:
            results.append([item_id, cnt, spam_cnt, normal_cnt, comment, 1])
    results = pd.DataFrame(data=results, columns=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'comment', 'is_spam'])
    return results


def load_data(data_path):
    """ load codebook, word embeddings, comment data and the stopword set. """
    file_codebook = open('/home/notebook/HaiQW/models/dictionary.pkl', 'rb')
    codebook = dict(pickle.load(file_codebook))
    word_embeddings = np.loadtxt('/home/notebook/HaiQW/models/embs.txt')
    orig_data = pd.read_csv(
        data_path,
        delimiter='|',
        error_bad_lines=False,
        header=None,
        names=['item_id', 'cnt', 'spam_cnt', 'normal_cnt', 'spam_comment', 'normal_comment'])
    orig_data = split_dataset(orig_data)
    stop_words = load_stopwords('/home/notebook/HaiQW/data/stopwords.txt')

    count = 0
    for idx in range(orig_data.shape[0]):
        count = count + 1
        if (count % 10000 == 0):
            print ('Cut comment %d' % count)
        comment = orig_data.loc[idx, 'comment']
        if comment:
            orig_data.loc[idx, 'comment'] = comment
            word_list1, word_list2 = jieba_fenci(comment, stop_words)
            orig_data.loc[idx, 'words1'] = ' '.join(word_list1)
            orig_data.loc[idx, 'words2'] = ' '.join(word_list2)
        else:
            orig_data.loc[idx, 'comment'] = np.nan
            orig_data.loc[idx, 'words1'] = np.nan
            orig_data.loc[idx, 'words2'] = np.nan

    # drop np.nan
    # clear_data = orig_data.dropna()
    return codebook, word_embeddings, orig_data


def load_word_embedding():
    """ load codebook, word embeddings, comment data and the stopword set. """
    file_codebook = open('/home/notebook/HaiQW/models/dictionary.pkl', 'rb')
    codebook = dict(pickle.load(file_codebook))
    word_embeddings = np.loadtxt('/home/notebook/HaiQW/models/embs.txt')
    return codebook, word_embeddings


def cal_deviation(comments):
    codebook, word_embeddings = load_word_embedding()
    clear_data = comments
    distance_matrix = pairwise_distances(word_embeddings, metric='cosine')
    vector = encode_sentence(clear_data['words2'], codebook, word_embeddings,
                             distance_matrix)
    remove_data = clear_data.dropna()

    item_deviation = np.zeros(shape=(np.unique(clear_data['item_id']).size, 2), dtype=float)
    cnt = 0
    for item_id in np.unique(clear_data['item_id']):
        test_data = remove_data.where(clear_data['item_id'] == item_id)
        test_indices = test_data.dropna().index
        test_std = np.nanstd(vector[test_indices])
        item_deviation[cnt, :] = [item_id, test_std]
        cnt += 1
        print("standard deviation of the item %d is %f" % (item_id, test_std)) 

    test_indices = clear_data.dropna().index
    test_std = np.nanstd(vector[test_indices])
    print("standard deviation of the item %d is %f" % (item_id, test_std)) 
    return item_deviation
    

def main():
    # spam_data_path = '/home/notebook/HaiQW/data/spam_items_20171016.csv' # 7万多条记录
    # normal_data_path = '/home/notebook/HaiQW/data/normal_items_20171016.csv' 
    normal_data_path = '/home/notebook/HaiQW/data/data_20171016/normal_items_5k.csv' # 6万多条记录
    # spam_item_deviation = cal_deviation(spam_data_path)
    normal_item_deviation = cal_deviation(normal_data_path)
    # print ("spam item deviation %f" % np.nanstd(spam_item_deviation))
    print ("normal item deviation %f" % np.nanmean(normal_item_deviation[:,1]))
    # return spam_item_deviation, normal_item_deviation
    

if __name__ == '__main__':
    main()
