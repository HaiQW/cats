#from __future__ import absolute_import, division, print_function
import jieba
import pandas as pd
import numpy as np



class Comment(object):
    __slots__ = ['pos_seg_num', 'neg_seg_num', 'seg_num']

    def __init__(self, pos_seg_num, neg_seg_num, seg_num):
        self.pos_seg_num = pos_seg_num
        self.seg_num = seg_num
        self.neg_seg_num = neg_seg_num
      

class Commodity(object):
    __slots__ = ['pos_seg_num_per_comment', 'neg_seg_num_per_comment']

    def __init__(self, comments):
        self.pos_seg_num_per_comment = sum([x.pos_seg_num for x in comments]) / float(len(comments)) if len(comments) != 0 else None
        self.neg_seg_num_per_comment = sum([x.neg_seg_num for x in comments]) / float(len(comments)) if len(comments) != 0 else None
        




def get_comments(comment_string_list, pos_words, neg_words):
    comments = []
    for comment_string in comment_string_list:
        pos_num, neg_num, seg_num = 0, 0, 0
        for word in jieba.cut(comment_string):
            if word.isspace():
                continue
            if word in pos_words:
                pos_num += 1
            if word in neg_words:
                neg_num += 1
            seg_num += 1

        if seg_num != 0:
            comments.append(Comment(pos_num, neg_num, seg_num))

    return comments


def cal_pos_neg_num(comments, pos_words, neg_words):
    for word in pos_words:
        jieba.add_word(word)
    for word in neg_words:
        jieba.add_word(word)
    
    data_size = comments.shape[0]
    results = np.zeros(shape=(data_size, 3), dtype=float)
    
    count = -1
    for comment_idx in range(0, data_size):
        count += 1
        item_id = comments.loc[comment_idx, 'item_id']
        comments_object_list = get_comments(comments.loc[comment_idx, 'comment'].split('/'), pos_words, neg_words)
        commodity = Commodity(comments_object_list)
        results[count, 0] = float(item_id)        
        results[count, 1] = commodity.pos_seg_num_per_comment
        if commodity.pos_seg_num_per_comment and commodity.neg_seg_num_per_comment:
            results[count, 2] =  commodity.pos_seg_num_per_comment - commodity.neg_seg_num_per_comment
        else:
            results[count, 2] = None
    return results