# CATS
The prototype system of CATS.  
The dataset evaluated by CATS is too large 
and cannot be opensourced in github, 
and thus this dataset will be avaliable upon one's request.


# Usage of CATS
In this version, CATS only provides the core functionaly modular, e.g., 
the feature extractor and data preprocessor. Several other functions will be 
coming soon!

Based on the prepared features, your can train your own 
spam calssifier. 

Refer prepare_data.ipynb for extracting features.

