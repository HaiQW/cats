#-*- encoding:utf-8 -*-
import pandas as pd
import numpy as np

import word_level_features
import semantic_features
import similarity_analysis
import pos_neg_features

from semantic_features import cal_single_comment_sentiment, cal_item_comments_sentiments
from word_level_features import cal_unique_words_ratio, cal_ngram_ratios, cal_punctuation_ratios, cal_entropy, cal_average_comment_length, cal_average_entropy, cal_average_punctuation_ratios
from similarity_analysis import cal_deviation
from pos_neg_features import cal_pos_neg_num


def prepare_features(orig_dataset, splitted_dataset, grams, pos_words, neg_words):
    """ prepare features for item comments.
        comments1: item with all comments in 
        comments2:
        comments3: 
    """
    
    results = pd.DataFrame(np.array(orig_dataset['item_id'].unique()), columns=['item_id'])
    print('prepare cnt')
    for index in range(0, orig_dataset.shape[0]):
        item_id = orig_dataset.loc[index, 'item_id']
        results.loc[results['item_id'] == item_id, 'cnt'] = orig_dataset.loc[index, 'cnt'] 
    
    #average positive/negtive  word number per comment for a commodity
    print('prepare average positive/negtive number per comment for a commodity')
    pos_neg_num = cal_pos_neg_num(orig_dataset, pos_words, neg_words)
    for index in range(0, pos_neg_num.shape[0]):
        item_id = pos_neg_num[index, 0]
        results.loc[results['item_id'] == item_id, 'avg_pos_word'] = pos_neg_num[index, 1] 
        #esults.loc[results['item_id'] == item_id, 'avg_neg_word'] = pos_neg_num[index, 2]
        results.loc[results['item_id'] == item_id, 'avg_pos/neg_gap_word'] = pos_neg_num[index, 2]
        
    # unique words ratio 
    print('prepare unique word ratios')
    unique_words_ratio = cal_unique_words_ratio(orig_dataset)
    for index in range(0, unique_words_ratio.shape[0]):
        item_id = unique_words_ratio[index, 0]
        value = unique_words_ratio[index, 1]
        results.loc[results['item_id'] == item_id, 'unique_word_ratios'] = value
        
    # average comment sentiments
    print('prepare average sentiments')
    tmp_comment = pd.DataFrame(np.array(splitted_dataset.dropna()), columns=splitted_dataset.columns)
    sentiment = cal_item_comments_sentiments(tmp_comment)
    for index in range(0, sentiment.shape[0]):
        item_id = sentiment[index, 0]
        value = sentiment[index, 1]
        results.loc[results['item_id'] == item_id, 'average_sentiments'] = value        

    # calculate ngram ratios
    print('prepare ngram_ratios and ngram_cnt')
    ngrams = cal_ngram_ratios(orig_dataset, grams)
    for index in range(0, ngrams.shape[0]):
        item_id = ngrams[index, 0]
        results.loc[results['item_id'] == item_id, 'ngram_ratios'] = ngrams[index, 2]
        results.loc[results['item_id'] == item_id, 'ngram_cnt'] = ngrams[index, 1] 

    # calculate punctuation ratios
    print('prepare p_ratios and p_cnt')
    punctuation_ratios = cal_average_punctuation_ratios(splitted_dataset)
    for index in range(0, punctuation_ratios.shape[0]):
        item_id = punctuation_ratios[index, 0]
        results.loc[results['item_id'] == item_id, 'p_ratios'] = punctuation_ratios[index, 2]
        results.loc[results['item_id'] == item_id, 'p_cnt'] = punctuation_ratios[index, 1]  
   
    #print ('prepare comment deviations')
    #deviations =  cal_deviation(comments3)
    #for index in range(0, deviations.shape[0]):
    #    item_id = deviations[index, 0]
    #    results.loc[results['item_id'] == item_id, 'deviation'] = deviations[index, 1]  
   
    # calculate comments entropy
    print('prepare average comment entropy')
    entropy = cal_average_entropy(splitted_dataset)
    for index in range(0, entropy.shape[0]):
        item_id = entropy[index, 0]
        results.loc[results['item_id'] == item_id, 'entropy'] = entropy[index, 1]    
           
    # calculate comment length
    print('prepare ave_length and sum_length')
    avg_length = cal_average_comment_length(splitted_dataset)
    for index in range(0, avg_length.shape[0]):
        item_id = avg_length[index, 0]
        results.loc[results['item_id'] == item_id, 'avg_length'] = avg_length[index, 1]        
        results.loc[results['item_id'] == item_id, 'sum_length'] = avg_length[index, 2] 
       
    
    return results
    