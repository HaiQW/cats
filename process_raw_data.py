#-*-coding:utf-8 -*-
#!/usr/bin/python
#****************************************************************#
# ScriptName: process_raw_data.py
# Author: hq_weng@zju.edu.cn
# Create Date: 2018-01-05 03:24
# Modify Author: hq_weng@zju.edu.cn
# Modify Date: 2018-01-05 03:24
# Function: process the e-commerce raw comments data 
#***************************************************************#
import json

import pandas as pd
import numpy as np


class RawDataProcess():
    def __init__(self, fname):
        self.fname = fname
        self.data = pd.read_csv(fname, chunksize=50000, low_memory=False)
        self.features = [
            'id', 'referenceId', 'pageId', 'creationTime', 'content'
        ]
        self.saved_features = [
            'item_id', 'cnt', 'spam_cnt', 'normal_cnt', 
            'spam_content', 'normal_content'
        ]
        self.saved_feature_types = {
            'item_id': 'int',
            'cnt': 'int',
            'spam_cnt': 'int',
            'normal_cnt': 'int',
            'spam_content' :'str',
            'normal_cnt': 'str'
        }
   
    def _process_chunk(self, chunk_data):
        formatted_chunk = pd.DataFrame(columns=self.saved_features)
        item_ids = chunk_data['referenceId'].drop_duplicates()
        for item_id in np.array(item_ids):
            item_id = str(item_id)
            comment_list = chunk_data.query(
                'referenceId == \'%s\'' % item_id)['content'].astype(str).tolist()
            comment_list = [comment.replace('\r\n', ' ') for comment in comment_list]
            comment_list = [comment.replace('\n', ' ') for comment in comment_list]
            formatted_chunk = formatted_chunk.append(
                pd.DataFrame(
                    [[
                        item_id, 
                        len(comment_list), 0, 0, 'null', 
                        '/'.join(map(str, comment_list))
                    ]], 
                    columns=self.saved_features),
                ignore_index=True
            )
        return formatted_chunk

    def to_formatted_comments(self):
        cnt = 0
        formatted_comments = pd.DataFrame(columns=self.saved_features)
        for chunk in self.data:
            print ('process file chunk: %d' % cnt)
            cnt += 1
            formatted_chunk = self._process_chunk(chunk)
            formatted_comments = formatted_comments.append(
                formatted_chunk, 
                ignore_index=True)

        # combine the results of each chunk
        print ('------------------------------------------------')
        print ('combine the results of each file chunk.')
        item_ids = formatted_comments['item_id'].drop_duplicates()
        results = pd.DataFrame(columns=self.saved_features)
        values = np.vstack(formatted_comments['item_id'].value_counts().index)
        counts = np.vstack(formatted_comments['item_id'].value_counts().tolist())
        value_counts = np.append(values, counts, axis=1)
        for idx in np.where(value_counts[:, 1] > 1)[0]:
            combined_id = value_counts[idx, 0]
            query_result = formatted_comments.query('item_id == %d' % combined_id)
            comment_list = query_result['normal_content'].tolist()
            cnt_list = query_result['cnt'].tolist()
            index_list = query_result.index
            formatted_comments.drop(index_list, inplace=True)
            formatted_comments = formatted_comments.append(
                pd.DataFrame(
                    [[
                        combined_id, 
                        np.sum(cnt_list), 0, 0, 'null', '/'.join(comment_list)
                    ]], 
                    columns=self.saved_features),
                ignore_index=True,
            )
        return formatted_comments
        
if __name__ == '__main__':
    fname = 'path/to/file'
    data_process = RawDataProcess(fname)
    comments = data_process.to_formatted_comments()
    comments.to_csv(fname + '.formatted', sep='|', index=False)

